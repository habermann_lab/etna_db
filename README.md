# evo-MOTiF database


## How to fill the database

If you want to file the database, please refer to the MySQL tables represented here. For now, the data already stored in the database are found in the folder db_content, and a sql file is already present with the code to help you fill it according to these files. Nevertheless, the PDB, PNG and alignment files are so big that they are stored independently from the database code, be carefull when you modify something.

![Alt text](pics/slimDatabase.png)