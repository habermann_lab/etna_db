<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>evo-MOTiF database</title>
        <link rel="stylesheet"  href="style.css">
        <!-- Make references as a numeral list and the pipeline as a dotted list-->
        <style>
			ul.pipeline {
				list-style: lower-alpha;
				}
			ul.references {
				list-style: number;
				}
        </style>
    </head>
 
    <body id="first_page">
 
   	<!-- Include the header -->
    <?php include("head.php"); ?>
    
    <!-- Include the tabs -->
    <?php include("menus.php"); ?>
    
    <!-- Main page -->
    <div id="corps">
        <h2>evo-MOTiF</h2>
        
        <p>
			evo-MOTiF is a pipeline we developed in order to make easier the study of SLiMs (REF).
        </p>
        
        <p>
			The goal of our pipeline is to study the principal properties of known motifs which can be 
			found on 
			<a target='_blank' rel='noopener noreferrer' href="http://elm.eu.org/">elm.eu.org</a>, 
			the Eukaryotic Linear Motif database [<a href="contact.php#references">2</a>], 
			in which we can find SLiMs that are experimentally validated, cross-linking associated with 
			mass spectrocopy data (such as Liu Fan et al. 2017 article [<a href="contact.php#references">3</a>]) 
			and post-translationnal data from <a target='_blank' rel='noopener noreferrer' href="https://www.phosphosite.org/homeAction.action">PhosphoSitePlus</a> [<a href="contact.php#references">4</a>].
        </p>
        <p>
			In order to do that, we implemented the pipeline shown below :
		</p>
        <p>
			<img src="pics/evoSLIM-pipeline.png" alt="pipeline" style="width:50%;height:50%;" class="center">
        </p>
        <p>
			<ul class="pipeline">
				<li> A fasta sequence is submitted to the pipeline to find its orthologs by the <b>Reciprocal 
				Best Hits</b> methods [<a href="contact.php#references">5</a>] or <b>MMseqs2</b> [<a href="contact.php#references">6</a>] against RefSeq, or using <b>OrthoDB</b> programmatic access [<a href="contact.php#references">7</a>].
				</li>
				
				<li> The orthologs are submitted to <b>MAFFT version 7</b> [<a href="contact.php#references">8</a>] to process a multiple sequence alignment.
				</li>
				<li><b>SLiMSuite</b> [<a href="contact.php#references">9</a>] is used to convert the sequences identifiers 
				and, combined with a list of SLiMs and IUpred [<a href="contact.php#references">10</a>,<a href="contact.php#references">11</a>], SLiMs are predicted. 
				This tool enables the calculation of disorder, conservation and surface accessibility scores.
				</li>
				<li> The results are statistically studied through a R script in order to study these scores and to decipher a pattern of conservation according to the structure of the motif.
				</li>
				<li> In the multiple sequence alignment, orthologs having the motif are extracted and their lineage is studied to find a taxonomic order in which the motif is found specifically.
				</li>
				<li> Optionally, a Maximum Likelihood [<a href="contact.php#references">12</a>] tree can be constructed using <b>PhyML</b> [<a href="contact.php#references">13</a>] 
				or <b>IQTree</b> version 2 [<a href="contact.php#references">14</a>].
				</li>
			</ul>
        </p>
        <p>
			The pipeline can be downloaded here : [link]
        </p>
        
        <h2>SLiMs-SCAR</h2>
			<p>
				To extract structural information about the motifs, we developped a small pipeline we called 
				SLiMs-SCAR (<B><U>S</U></B>hort <B><U>Li</U></B>near <B><U>M</U></B>otif<B><U>s</U></B> <b><u>S</u></b>tru<b><u>C</u></b>ture extr<b><u>A</u></b>cto<b><u>R</u></b>).<br>
				<p>
					<img src="pics/scar_figure.png" alt="pipeline" style="width:50%;height:50%;" class="center">
				</p>
				The pipeline takes as input data motifs from evo-MOTiF (ref) or from <a target='_blank' rel='noopener noreferrer' href="http://elm.eu.org/">elm.eu.org</a> 
				[<a href="contact.php#references">2</a>], and fasta sequences associated with them (ELM-SCAR).<br>
				Using the submodule <b>PDB</b> from Biopython [<a href="contact.php#references">15</a>], the corresponding PDB files are downloaded. One by one, they are submitted to our parser, 
				the tool looks for the motif in it and a secondary structure string is extracted, using the DSSP codification [<a href="contact.php#references">16</a>].<br>
				Then, using <a href="https://www.biotite-python.org/">Biotite</a> [<a href="contact.php#references">17</a>], a 2D representation of the secondady structures of the motifs is generated.<br>
				Finally, a new PDB file is generated, containing only the motif structure with the use of <a target='_blank' rel='noopener noreferrer' href="http://www.bonvinlab.org/pdb-tools/">pdb-tools</a> [<a href="">18</a>].<br>
				On evo-MOTiF database, 3D motifs are visualized using JSmol [<a href="contact.php#references">19</a>].
			</p>
        
    </div>
    
    <!-- Include the foot -->
    <?php include("foot.php"); ?>
    
    </body>
</html>
