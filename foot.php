<!DOCTYPE html>
<html>
	<head>
		<link href="foot.css" rel="stylesheet" />
		<!-- Footer of each page-->
	</head>

 
	<body>
		<nav id="pdep">
			<div>
				<form>
					<b><input type="button" value="Go back" onclick="history.back()" style="height:30px; width:70px" /></b>
				</form>
			</div>
			<section id="logos">
				<div class="col-md-12">
					<a href ="https://sciences.univ-amu.fr/"><img src="pics/Aix-Marseille_Univ_logo1.png" height="80"></a>
					<a href ="http://www.cnrs.fr/en"><img src="pics/1200px-CNRS.svg.png" height="80"></a>  
					<a href ="http://www.ibdm.univ-mrs.fr"><img src="pics/ibdm-logo.png" height="100"></a>
				</div>
			</section>
			<p>
			<?php
				session_start(); // Should always be on top
				if(!isset($_SESSION['counter'])) { // It's the first visit in this session
					$handle = fopen("counter.txt", "r");
					if(!$handle){ 
						echo "Could not open the file" ;
					} else { 
						$counter = ( int ) fread ($handle,20) ;
						fclose ($handle) ;
						$counter++ ; 
						echo" <B> Number of visitors so far : </B>". $counter . " </p> " ; 
						$handle = fopen("counter.txt", "w" ) ; 
						fwrite($handle,$counter) ; 
						fclose ($handle) ;
						$_SESSION['counter'] = $counter;
					}
				} else { // It's not the first time, do not update the counter but show the total hits stored in session
					$counter = $_SESSION['counter'];
					echo" <B>Number of visitors so far : </B>". $counter . " </p> " ;
				}
			?>
			</p>
		</nav>
    </body>
</html>
