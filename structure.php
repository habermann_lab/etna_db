<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>evo-MOTiF database - Structures</title>
        <link rel="stylesheet"  href="style.css">
        <link rel="stylesheet"  href="slim.css">
        <script src="excellentexport-1.4/excellentexport.js"></script>
        <!-- Make references as a numeral list -->
        <style>
			ul.ref {
				list-style: number;
				}
        </style>
    </head>
 
    <body id="first_page">
 
	<!-- Include the header -->
    <?php include("head.php"); ?>
    <!-- Include the tabs -->
    <?php include("menus.php"); ?>
    
    <div id="corps">
		<br>
		
		<form method="GET">
			
			<?php
				// Extract the ELM ids to create a list
				$connect = new mysqli('localhost', 'etnadb', 'pldbssr_2021','slimDatabase');
				$query = 'SELECT DISTINCT(accession) FROM motifs_structures';
				$result = $connect->query($query);
				
				// Store the ELM ids in an array
				$arraySlims = array();
				foreach($result as $row) {
					if (substr( $row['accession'], 0, 4 ) !== "XLMS" and substr( $row['accession'], 0, 3 ) !== "PSP"){
						array_push($arraySlims,$row['accession']);
					}
				}
				$selected = '';
				asort($arraySlims);
				
				// Go through the array to create the list
				echo '<select name="search" id="search">',"n";
				foreach($arraySlims as $id_array => $motifs_id){
					if($motifs_id === 'DEG_Kelch_Keap1_1'){
						$selected = ' selected="selected"';
					}
					echo "\t",'<option value="', $motifs_id ,'"', $selected ,'>', $motifs_id ,'</option>',"\n";
					$selected='';
				}
				echo '</select>',"\n";
			?>
			
			<input id="submit" type="submit" value="Search">
			
			<button id="download" type="button" ><a download="etna_structures.csv" href="#" onclick="return ExcellentExport.csv(this, 'table_motifs');" id="downloader">Export to csv</a></button>
			
			<?php

				//Connexion to the MySQL database
				$connect = new mysqli('localhost', 'etnadb', 'pldbssr_2021','slimDatabase');
				if (isset($_GET["search"])) {
					$ELM_id = $_GET['search'];
					}
				else {
					$ELM_id = "DEG_Kelch_Keap1_1" ; // default value for the menu
					}
				
				// select all of the motifs from the corresponding ELM id
				$query = 'SELECT * FROM elm_classes WHERE slims = "'.$ELM_id.'"';
				$result = $connect->query($query);
				$row = $result->fetch_assoc();
				$regex = $row['regex'];
				$desc = $row['description'];
				
				$query = 'SELECT * FROM motifs_structures WHERE accession = "'.$ELM_id.'"';
				$result = $connect->query($query);
				echo $result->num_rows . " entries for <b>" .$ELM_id . "</b>.<br>Click on the <b>Structure representation</b> links to access to their 2D and 3D representations<br><br>";
				
				echo "<div id='regex'><b>Regular expression <span id='pluspetit'>[<a href='contact.php#references'>2</a>]</span> : &nbsp;&nbsp;&nbsp;</b><i>" . $regex . "</i></div><br>" ;
				$structures = array();
				$c = 0;
				// table creation
				echo '<table id="table_motifs"><thead><tr>
					<th>ELM accession</th>
					<th>Regular expression</th>
					<th>PDB</th>
					<th>UniProt</th>
					<th>EMD id</th>
					<th>Start</th>
					<th>End</th>
					<th>Motif sequence</th>
					<th>Structure</th>
					<th>Structure representation</th>
					</tr></thead>';
				// read each motif and put them on a line of the table
				while ($row = $result->fetch_assoc()) {
					//Create a link to these motifs
					$png_name = "png/".$row['uniprot'] . "_" .$row['PDB_id'].  "_" .$row['start_pos'] . "_" .$row['end_pos'].".png";
					$pdb_name = "Motifs_PDB/".$row['uniprot'] . "_" .$row['PDB_id'].  "_" .$row['start_pos'] . "_" .$row['end_pos'].".pdb";
					// retrieve the corresponding id for each motif
					$query2 = 'SELECT id FROM Slims WHERE uniprot = "'.$row['uniprot'].'" AND start_pos = '.$row['start_pos'] . ' AND end_pos = '.$row['end_pos'] . ' AND motifs = "'.$row['motif_sequence']. '"';
					$result2 = $connect->query($query2);
					$row2 = $result2->fetch_assoc();
					$etna_id = $row2['id'];
					echo "<tr><td>";
					if (substr( $row['accession'], 0, 4 ) === "XLMS" or substr( $row['accession'], 0, 3 ) === "PSP") {
						$query2 = 'SELECT id FROM Slims WHERE slims = "'.$row['accession'].'" AND motifs="' .$row['motif_sequence']. '"';
						$result2 = $connect->query($query2);
						$row2 = $result2->fetch_assoc();
						echo "<a target='_blank' rel='noopener noreferrer' href='slim.php?id=" . $row2['id'] ."'>" . $row['accession'] . "</a>";
					} else {
						echo "<a target='_blank' rel='noopener noreferrer' href='http://elm.eu.org/elms/" . $row['accession'] ."'>" .$row['accession']."</a></td>";
					}
					array_push($structures, $row['structure_sequence'] );
					echo "<td>" . $regex . "</td>";
					echo "<td><a target='_blank' rel='noopener noreferrer' href='https://www.rcsb.org/structure/".
						$row['PDB_id']. "'>".$row['PDB_id']."</td><td>".$row['uniprot']."</td><td><a href='slim.php?id=" .$etna_id . "'>".$etna_id."</a></td><td>".$row['start_pos']."</td><td>".
						$row['end_pos']."</td><td>".$row['motif_sequence']."</td><td>".$row['structure_sequence'] . "</td>";
					echo "<td>
							<a href='pdb_motif.php?id=".$row['uniprot'] . "_" .$row['PDB_id'].  "_" .$row['start_pos'] . "_" .$row['end_pos'].".pdb' >".$row['uniprot'] . "_" .$row['PDB_id'].  "_" .$row['start_pos'] . "_" .$row['end_pos'].".pdb</a>
						</td>";
					echo "</tr>";
				}
				echo '</table>';
				
			?>

		</form>
		
	</div>
    
    <!-- Include the foot -->
    <?php include("foot.php"); ?>
    
    </body>
</html>
