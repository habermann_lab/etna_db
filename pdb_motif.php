<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
         <?php
			$id = $_GET['id'];
        
			echo '<title>evo-MOTiF database - '.$id.'</title>';
        ?>
        <link rel="stylesheet"  href="style.css">
        <link rel="stylesheet"  href="slim.css">
        <script src="excellentexport-1.4/excellentexport.js"></script>
        <script type="text/javascript" src="jmol-14.31.39/jsmol/JSmol.min.js"></script>
        <!-- Make references as a numeral list -->
        <style>
			ul.ref {
				list-style: number;
				}
        </style>
    </head>
 
    <body id="first_page">
 
	<!-- Include the header -->
    <?php include("head.php"); ?>
    <!-- Include the tabs -->
    <?php include("menus.php"); ?>
    
    <div id="corps">
		<p>
		<?php
			//Connexion to the MySQL database
			$connect = new mysqli('localhost', 'etnadb', 'pldbssr_2021','slimDatabase');
			
			// retrieve the protein, pdb and positions of the motif from the url
			$id = $_GET['id']; //motif PDB id
			$exploded = explode('_',$id);
			$uniprot_id = $exploded[0];
			$pdb_id = $exploded[1];
			$start_pos = intval($exploded[2]);
			$end_pos = intval($exploded[3]);
			
			// select the structure information associated with this specific motif from the database
			$query = 'SELECT * FROM motifs_structures WHERE uniprot = "'.$uniprot_id . '" AND PDB_id = "' .$pdb_id . '" AND start_pos = '.$start_pos . ' AND end_pos = '.$end_pos;
			$result = $connect->query($query);
			$row = $result->fetch_assoc();
			$pdb_link = "Motifs_PDB/" . $id ; // location of the PDB file
			$png_name = "png/" . $uniprot_id. "_" . $pdb_id. "_" . $start_pos. "_" . $end_pos . ".png"; // location of the 2D representation of the motif
			$elm_id = $row['accession'];
			$sequence = $row['motif_sequence'];
			
			echo "<h2>Structure of " . $elm_id. " in ".$uniprot_id." : <i>" .$row['structure_sequence']. "</i></h2>" ;
			
			// Connect to uniprot to extract the species name
			$xml_file = 'https://www.uniprot.org/uniprot/' .$row['uniprot']. '.xml';
			$xml = simplexml_load_file($xml_file);
			$array = json_decode(json_encode((array)$xml), TRUE);
			if (is_array($array["entry"]["organism"]["name"])) {
				echo "The corresponding motif is <b>" . $elm_id ."</b> from <b><a target='_blank' rel='noopener noreferrer' href='https://www.uniprot.org/uniprot/" .$row['uniprot']. "'>" . $row['uniprot']."</a></b>, <i>".$array["entry"]["organism"]["name"]["0"]."</i>.</br>";
			} else {
				echo "The corresponding motif is <b>" .$elm_id ."</b> from <b><a target='_blank' rel='noopener noreferrer' href='https://www.uniprot.org/uniprot/" .$row['uniprot']. "'>" . $row['uniprot']."</a></b>, <i>".$array["entry"]["organism"]["name"]."</i>.</br>";
			}
			
			// Extract the id of the motif from the database to link the structure to the main page
			$query = 'SELECT Slims.id FROM Slims WHERE 
				Slims.uniprot = "'.$uniprot_id . '"
				AND Slims.motifs = "'. $sequence. '"
				AND Slims.start_pos = "'.$start_pos.'"
				AND Slims.end_pos = "'.$end_pos.'"';
			$result = $connect->query($query);
			$row = $result->fetch_assoc();
			if(is_null($row['id']))
			{
				echo "No corresponding motif in the database yet.";
			} else {
				echo "This motif is associated with the EMD id <a href='slim.php?id=" .$row['id'] . "'>".$row['id'] ."</a>." ;
			}
			
			//display 2D structure
			echo "<br><br></brW><div><img src='".$png_name."'></div>";
			
			// use of JSmol to display the 3D motif structure
			echo "<script type='text/javascript'>
			
						var myJmol = 'myJmol';
						var JmolInfo = {
							width:'100%',
							height:'100%',
							color:'#EBFCFC',
							src:'". $pdb_link ."',
							j2sPath:'jmol-14.31.39/jsmol/j2s',
							serverURL:'jmol-14.31.39/jsmol/php/jsmol.php',
							use:'html5',
							
						}
						$(document).ready(function(){
							$('#JmolDiv').html(
								Jmol.getAppletHtml(myJmol, JmolInfo)
							 );
							
							
						});
						
						</script>
							<div id='JmolDiv' style='width:70vmin; height:70vmin;' class='JmolPanels'></div>" ;
			
		?>
		
		<!-- Link to the complete structure of the protein -->
		<?php
			echo "<a href='pdb_motif_complete.php?id=" . $id . "' >Protein complete structure</a>";
		?>
		<br>
		
		
		<!-- Download link for the PDB file -->
		<button type='submit' onclick='window.open("<?= $pdb_link ?>")'>Download <i><?= $id ?></i></button>
		<br><br><br>
		<div>
			This viewer is generated using JSmol [<a href="contact.php#references">19</a>]. If you are not used to it, it is really simple to use.<br>
			Just simply right-click on the structure, you can change the <i>Style</i> to the <i>Scheme Cartoon</i> to have a better visualization 
			of the secondary structure.<br>
			You can also change the <i>Color</i>, <i>By Scheme</i>, <i>Secondary Structure</i>.
		</div>
	</div>
    
    <!-- Include the foot -->
    <?php include("foot.php"); ?>
    
    </body>
</html>

