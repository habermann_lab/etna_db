<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
		<?php
			$seq = (string) $_GET['seq'];
        
			echo '<title>evo-MOTiF - '.$seq.'</title>';
        ?>
        <link rel="stylesheet"  href="style.css">
        <link href="JSAV/external/jquery.css" rel="stylesheet" />
		<script type='text/javascript' src='JSAV/external/jquery-1.10.2.min.js'></script>
		<script type='text/javascript' src='JSAV/external/jquery-ui-1.10.4.custom.min.js'></script>
		<link href="JSAV/JSAV.css" rel="stylesheet" />
        <script src="JSAV/JSAV.js"></script>
        <script src="https://d3js.org/d3.v7.min.js"></script>
        <link rel="stylesheet"  href="slim.css">
        <link rel="stylesheet"  href="JSAV/JSAV.css">
        <script src="jquery-csv-main/src/jquery.csv.js"></script>
    </head>
 
    <body id="first_page">
 
	<!-- Include the header -->
    <?php include("head.php"); ?>
    
    <!-- Include the tabs -->
    <?php include("menus.php"); ?>
    
    <!-- Main page -->
    <p>
		
		<?php
			$seq = (string) $_GET['seq'];
			$file_tsv = 'align/seq_UNK__' .$seq. '.tsv';
			$file_fasta = 'align/seq_UNK__' .$seq. '.fasta';
			echo "<h2>" . $seq . " multiple sequence alignment </h2>";
			$file = file_get_contents($file_tsv);
			if (trim($file) == true) {
				
				//Create the space for the alignment output
				echo '<div id="MSA_field"></div>' ;
				echo '<pre id="fasta-file"> <div id="fasta_content"> id&nbsp;sequence<br>' .$file. '</div></pre>';
				
			}
		?>
		
		<script>
			// function to transform a CSV loaded in a div to a javascript array
			function csv_To_Array(str, delimiter = "\t") {
		      const header_cols = ["id","sequence"];
		      const row_data = str.slice(str.indexOf("\n") + 1).split("\n");
		      const arr = row_data.map(function (row) {
		        const values = row.split(delimiter);
		        const el = header_cols.reduce(function (object, header, index) {
		          object[header] = values[index];
		          return object;
		        }, {});
		        return el;
		      });
		
		      // return the array
		      return arr;
		    }
				// Read the MSA tsv and convert it to an array
				const reader = new FileReader();
				const text = document.getElementById("fasta_content").innerText;
				const data = csv_To_Array(text);
				// remove empty lines
				//document.getElementById("MSA_field").innerHTML = JSON.stringify(data, null, 2);
				data.pop();
				data.pop();
				//document.getElementById("MSA_field").innerHTML = JSON.stringify(data, null, 2);
				// Add the options to the JSAV display and print the alignment
				var options = Array();
				options.sortable = true;
				options.selectable = true;
				options.hideable = true;
				options.deletable = true;
				options.consensus = true;
				options.scrollX = "100%";
				options.scrollY = "300px";
				options.fasta = true;
				options.selectColor = true;
				options.colorChoices = ["clustal", "taylor", "zappo", "hphob", "helix", "strand", "turn", "buried"];
				options.colourScheme = "clustal";
				printJSAV("MSA_field",data,options);
				document.getElementById("fasta_content").style.display = "none";
		</script>
		
    </p>
    
    <!-- Include the foot -->
    <?php include("foot.php"); ?>
    
    </body>

</html>
