<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>evo-MOTiF database</title>
        <!-- Load needed CSS files to make the datatable-->
        <link rel="stylesheet"  href="style.css">
		<link rel="stylesheet"  href="DataTables/css/jquery.dataTables.min.css">	
		<link rel="stylesheet"  href="style_dt.css">
		<link rel="stylesheet"  href="DataTables/css/buttons.dataTables.min.css">
		<link rel="stylesheet"  href="Buttons/css/buttons.bootstrap.min.css">
		<link rel="stylesheet"  href="Buttons/css/buttons.bootstrap4.min.css">
		<link rel="stylesheet"  href="Buttons/css/buttons.foundation.min.css">
		<link rel="stylesheet"  href="Buttons/css/buttons.jqueryui.min.css">
		<link rel="stylesheet"  href="Buttons/css/buttons.semanticui.min.css">
		<link rel="stylesheet"  href="jquery/colReorder.dataTables.min.css">
		<link rel="stylesheet"  href="jquery/colReorder.bootstrap.min.css">
		<link rel="stylesheet"  href="jquery/colReorder.bootstrap4.min.css">
		<link rel="stylesheet"  href="jquery/fixedColumns.dataTables.min.css">
		<!-- Load needed JavaScript files to make the datatable-->
		<script src="jquery/jquery-3.5.1.js" type="text/javascript"></script>
		<script src="DataTables/js/jquery.dataTables.min.js" type="text/javascript"></script> 
		<script src="jquery/dataTables.fixedColumns.min.js" type="text/javascript"></script> 
		<script src="Buttons/js/dataTables.buttons.min.js" type="text/javascript"></script>
		<script src="Buttons/js/buttons.flash.min.js" type="text/javascript"></script>
		<script src="Buttons/js/buttons.html5.min.js" type="text/javascript"></script>
		<script src="Buttons/js/buttons.print.min.js" type="text/javascript"></script>
		<script src="jquery/dataTables.select.min.js" type="text/javascript"></script>
		<script src="jquery/pdfmake.min.js" type="text/javascript"></script>
		<script src="jquery/vfs_fonts.js" type="text/javascript"></script>	
		<script src="Buttons/js/buttons.bootstrap.min.js" type="text/javascript"></script>	
		<script src="jquery/jszip.min.js" type="text/javascript"></script>
		<script src="Buttons/js/buttons.colVis.min.js" type="text/javascript"></script>	
		<script src="Buttons/js/buttons.foundation.min.js" type="text/javascript"></script>	
		<script src="Buttons/js/buttons.jqueryui.min.js" type="text/javascript"></script>	
		<script src="Buttons/js/buttons.semanticui.min.js" type="text/javascript"></script>
		<script src="jquery/buttons.colVis.min.js" type="text/javascript"></script>
		<script src="jquery/dataTables.colReorder.min.js" type="text/javascript"></script>
		<script src="jquery/colReorder.bootstrap.min.js" type="text/javascript"></script>
		<script src="jquery/colReorder.bootstrap4.min.js" type="text/javascript"></script>
		<!-- Needed to avoid the first column to have a scroll bar as it's fixed -->
		<style>
			.DTFC_LeftBodyLiner {overflow: hidden;}
			.DTFC_LeftFootWrapper {display:none;}
			tfoot input {
				width: 100%;
				padding: 3px;
				box-sizing: border-box;
			}
		</style>
</head>

<body id="first_page">
	<!-- Include the header -->
    <?php include("head.php"); ?>
    
    <!-- Include the tabs -->
    <?php include("menus.php"); ?>
    
    <!-- Main page -->
	
	<p>
		Simply click on the rows you want to download and click on the export buttons.</br>
		Generic names where given to motifs from cross-linking data, with a XLMS prefix and 
		from PhosphositePlus with a PSP prefix.</br>
	</p>
	
	<p>
		<ol class="scores">
			<li><b>Positional conservation : </b>  0 remotely conserved &hArr; 1 conserved.</li>
		
			<li><b>Disorder : </b> 0 structured &hArr; 1 disordered.
			</li>
		
			<li><b>Surface accessibility : </b> 0 buried &hArr; 9 exposed.
			</li>
			
			<li><b>Overall conservation : </b> 0 remotely conserved &hArr; 1 conserved.
			</li>
			<li><b>Amino acid property conservation : </b> 0 remotely conserved &hArr; 1 conserved.
			</li>
		</ol>
	</p>
	
	<div class="container">
		<!-- Creation of the table with all its columns -->
		<table id="slims" class="display nowrap" cellspacing="0" width="100%">
		<thead>
			<tr>
			<th>EMD Id</th>
			<th>Uniprot Id</th>
			<th>SLiM</th>
			<th>Motif</th>
			<th>Start</th>
			<th>End</th>
			<th>Positional conservation score</th>
			<th>Disorder score</th>
			<th>Overall conservation score</th>
			<th>AA properties conservation score</th>
			<th>Surface accessibility score</th>
			<th>Taxonomy (taxid)</th>
			</tr>
		</thead>
		<tfoot>
            <tr>
            <th>EMD Id</th>
            <th>Uniprot Id</th>
			<th>SLiM</th>
			<th>Motif</th>
			<th>Start</th>
			<th>End</th>
			<th>Positional conservation  score</th>
			<th>Disorder score</th>
			<th>Overall conservation score</th>
			<th>AA properties conservation score</th>
			<th>Surface accessibility score</th>
			<th>Taxonomy (taxid)</th>
            </tr>
        </tfoot>
	</table>
	</div>
	
	<!-- Include the foot -->
	<?php include("foot.php"); ?>
	
</body>

<script>
$(document).ready(function() {
	// Setup - add a text input to each footer cell
    $('#slims tfoot th').each( function () {
        var title = $(this).text();
        $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
    } );
 
    // DataTable
        
	var selected = [];
    $('#slims').DataTable({
		initComplete: function () {
            // Apply the search
            this.api().columns().every( function () {
                var that = this;
 
                $( 'input', this.footer() ).on( 'keyup change clear', function () {
                    if ( that.search() !== this.value ) {
                        that
                            .search( this.value )
                            .draw();
                    }
                } );
            } );
        },
        // all datatable options
        "ajax": "server.php",
		"scrollX": true,
		"scrollY": "800px",
		"scrollCollapse": true,
		"pagingType": "full_numbers",
		"pageLength": 10,
        "processing": true,
        "serverSide": true,
        "dom": 'lBfrtip',
        "lengthMenu": [[10,25, 50, 100, -1], [10,25, 50, 100, "All"]],
        "bFilter": true,
        "buttons": [
            {
            extend: 'csv',
            text: 'Export to csv',
            exportOptions: {
                modifier: {
                    search: 'none'
                },
                columns: ':not(.notexport)',
            }
        },
            {
            extend: 'excel',
            text: 'Export to excel',
            exportOptions: {
                modifier: {
                    search: 'none'
                },
                columns: ':not(.notexport)',
            }
        },
            'selectAll',
            'selectNone',
            {
				extend: 'colvis',
				"bRestore": true,
				columnText: function ( dt, idx, title ) {
					return (idx+1)+': '+title;
					},
				text: 'Remove/Display columns',
				collectionTitle: 'Click on the columns you want to disappear',
                collectionLayout: 'two-column'
			},
			{
				extend: 'colvisRestore',
				text: 'Restore columns',
			},
        ],
        select: {
            style:    'multi',
            blurable: true
        },
        order: [[ 0, 'asc' ]],
        fixedColumns:   {
            leftColumns: 1
        },
		"columnDefs": [ {
			"targets": 0,
			"render": function ( data, type, row, meta ) {
				return '<a href="slim.php?id='+data+'">'+data+'</a>'; // add link to the main page of the motif
			}
		} ]
    } );    
} );
</script>

</html>
