<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
         <?php
			$id = $_GET['id'];
        
			echo '<title>evo-MOTiF database - '.$id.'</title>';
        ?>
        <link rel="stylesheet"  href="style.css">
        <link rel="stylesheet"  href="slim.css">
        <script src="excellentexport-1.4/excellentexport.js"></script>
        <script type="text/javascript" src="jmol-14.31.39/jsmol/JSmol.min.js"></script>
        <!-- Make references as a numeral list -->
        <style>
			ul.ref {
				list-style: number;
				}
        </style>
    </head>
 
    <body id="first_page">
 
	<!-- Include the header -->
    <?php include("head.php"); ?>
    <!-- Include the tabs -->
    <?php include("menus.php"); ?>
    
    <div id="corps">
		<p>
		<?php
			//Connexion to the MySQL database
			$connect = new mysqli('localhost', 'etnadb', 'pldbssr_2021','slimDatabase');
			
			$id = $_GET['id']; //motif PDB id
			$exploded = explode('_',$id);
			$uniprot_id = $exploded[0];
			$pdb_id = $exploded[1];
			// Motif position in the protein sequence !!!
			$start_pos = intval($exploded[2]);
			$end_pos = intval($exploded[3]);
			
			// select the structure information associated with this specific motif from the database
			$query = 'SELECT * FROM motifs_structures WHERE uniprot = "'.$uniprot_id . '" AND PDB_id = "' .$pdb_id . '" AND start_pos = '.$start_pos . ' AND end_pos = '.$end_pos;
			$result = $connect->query($query);
			$row = $result->fetch_assoc();
			$pdb_link = "Motifs_PDB/" . $id ;
			$png_name = "png/" . $uniprot_id. "_" . $pdb_id. "_" . $start_pos. "_" . $end_pos . ".png";
			$elm_id = $row['accession'];
			$sequence = $row['motif_sequence'];
			// Motif position in the PDB file !!!
			$pdb_chain = $row['chain_pdb'];
			$pdb_start = $row['start_pdb'];
			$pdb_end = $row['end_pdb'];
			echo "<h2>Structure of " . $elm_id. " in ".$uniprot_id." : <i>" .$row['structure_sequence']. "</i></h2>" ;
			
			// Connect to uniprot to extract the species name
			$xml_file = 'https://www.uniprot.org/uniprot/' .$row['uniprot']. '.xml';
			$xml = simplexml_load_file($xml_file);
			$array = json_decode(json_encode((array)$xml), TRUE);
			if (is_array($array["entry"]["organism"]["name"])) {
				echo "The corresponding motif is <b>" . $elm_id ."</b> from <b><a target='_blank' rel='noopener noreferrer' href='https://www.uniprot.org/uniprot/" .$row['uniprot']. "'>" . $row['uniprot']."</a></b>, <i>".$array["entry"]["organism"]["name"]["0"]."</i>.</br>";
			} else {
				echo "The corresponding motif is <b>" .$elm_id ."</b> from <b><a target='_blank' rel='noopener noreferrer' href='https://www.uniprot.org/uniprot/" .$row['uniprot']. "'>" . $row['uniprot']."</a></b>, <i>".$array["entry"]["organism"]["name"]."</i>.</br>";
			}
			
			// Extract the id of the motif from the database to link the structure to the main page
			$query = 'SELECT Slims.id FROM Slims WHERE 
				Slims.uniprot = "'.$uniprot_id . '"
				AND Slims.motifs = "'. $sequence. '"
				AND Slims.start_pos = "'.$start_pos.'"
				AND Slims.end_pos = "'.$end_pos.'"';
			$result = $connect->query($query);
			$row = $result->fetch_assoc();
			if(is_null($row['id']))
			{
				echo "No corresponding motif in the database yet.";
			} else {
				echo "This motif is associated with the EMD id <a href='slim.php?id=" .$row['id'] . "'>".$row['id'] ."</a>." ;
			}
			
			//display 2D structure
			echo "<br><br></brW><div><img src='".$png_name."'></div>";
			
		?>
		
		<!-- use of 3Dmol to display the 3D protein structure with the motif highlighted -->
		<script src="https://3Dmol.org/build/3Dmol-min.js" async></script>
		<?php
        echo '<div style="height: 800px; width: 800px; position: relative;" class="viewer_3Dmoljs" data-pdb="'.$pdb_id .'" data-backgroundcolor="0xffffff" data-style="cartoon" data-ui="true" data-select1="chain:'.$pdb_chain.';resi:'.$pdb_start.'-'.$pdb_end.'" data-style1="sphere:color=red"></div>';
        ?>
        
        <!-- Link to the motif structure only -->
		<?php
			echo "<a href='pdb_motif.php?id=" . $id . "' >Motif structure</a>";
		?>
		<br>
		<!-- Download link for the PDB file -->
		<button type='submit' onclick='window.open("<?= $pdb_link ?>")'>Download <i><?= $id ?></i></button>
		<br><br><br>
		
		<div>
			This viewer is generated using 3Dmol [<a href="contact.php#references">20</a>]. 
		</div>
	</div>
    
    <!-- Include the foot -->
    <?php include("foot.php"); ?>
    
    </body>
</html>

