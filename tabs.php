<?php
    function affiche_menu()
    {
		// Links of each tab
        $tab_menu_lien = array( "index.php", "database.php", "structure.php", "pipeline.php", "contact.php");
        // Name of each tab
        $tab_menu_texte = array( "Home", "Database", "Structure", "Pipeline", "About");
        
        //Page information
        $info = pathinfo($_SERVER['PHP_SELF']);
		
		//Display the menu
        $menu = "\n<div id=\"menu\">\n    <ul id=\"onglets\">\n";

        //Loop to create the tabs with each name and link
        foreach($tab_menu_lien as $cle=>$lien)
        {
            $menu .= "    <li";
            // If the file name exists, the tab is activated    
            if( $info['basename'] == $lien )
                $menu .= " class=\"active\"";
                
            $menu .= "><a href=\"" . $lien . "\">" . $tab_menu_texte[$cle] . "</a></li>\n";
        }
        
        $menu .= "</ul>\n</div>";
        
        // Return the menu for an HTML interpretation
        return $menu;        
    }
?>
