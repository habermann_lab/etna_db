<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <?php
			$id = (int) $_GET['id'];
        
			echo '<title>evo-MOTiF - '.$id.'</title>';
        ?>
        <link rel="stylesheet"  href="style.css">
        <link href="JSAV/external/jquery.css" rel="stylesheet" />
		<script type='text/javascript' src='JSAV/external/jquery-1.10.2.min.js'></script>
		<script type='text/javascript' src='JSAV/external/jquery-ui-1.10.4.custom.min.js'></script>
		<link href="JSAV/JSAV.css" rel="stylesheet" />
        <script src="JSAV/JSAV.js"></script>
        <script src="https://d3js.org/d3.v7.min.js"></script>
        <link rel="stylesheet"  href="slim.css">
        <link rel="stylesheet"  href="JSAV/JSAV.css">
        <script src="jquery-csv-main/src/jquery.csv.js"></script>
    </head>
 
    <body id="first_page">
 
	<!-- Include the header -->
    <?php include("head.php"); ?>
    
    <!-- Include the tabs -->
    <?php include("menus.php"); ?>
    
    <!-- Main page -->
    <p>
		<?php
			//Connexion to the MySQL database
			$connect = new mysqli('localhost', 'etnadb', 'pldbssr_2021','slimDatabase');
			$id = (int) $_GET['id'];
			
			//Select the uniprot Id of the protein
			$query = 'SELECT uniprot FROM Slims WHERE id = '.$id;
			$result = $connect->query($query);
			$row = $result->fetch_assoc();
			$unip = (string) $row['uniprot'];
			
			//Create boxes for each categories, create link to uniprot
			echo '<fieldset id="bigboxes"> <legend id="smallboxes">About the protein</legend>
				<b>UniprotKB AC : </b> <a target="_blank" rel="noopener noreferrer" href="https://www.uniprot.org/uniprot/' .$row['uniprot']. '">' .$row['uniprot']. '</a></br>';
			//Extract informations about the protein on uniprot
			$xml_file = 'https://www.uniprot.org/uniprot/' .$row['uniprot']. '.xml';
			$xml = simplexml_load_file($xml_file);
			$array = json_decode(json_encode((array)$xml), TRUE);
			//Extract the refseq Id
			$query = 'SELECT refseq FROM uniprot_refseq WHERE uniprot = (SELECT uniprot FROM Slims where id = ' .$id. ')';
			$result = $connect->query($query);
			$row = $result->fetch_assoc();
			echo '<b>RefSeq : </b><a target="_blank" rel="noopener noreferrer" href="https://www.ncbi.nlm.nih.gov/protein/' .$row['refseq']. '">' .$row['refseq']. '</a></br>';
			$refseq_id = (string) $row['refseq'];
			if (array_key_exists('recommendedName', $array["entry"]["protein"])) {
				echo '<b>Name : </b>' .$array["entry"]["name"].' - '.$array["entry"]["protein"]["recommendedName"]["fullName"].'</br>';
			} else {
				echo '<b>Name : </b>' .$array["entry"]["name"].' - '.$array["entry"]["protein"]["submittedName"]["fullName"].'</br>';
			}
			if (is_array($array["entry"]["organism"]["name"])) {
				echo '<b>Species : </b><i>' .$array["entry"]["organism"]["name"]["0"]. '</i></br>';
			} else {
				echo '<b>Species : </b><i>' .$array["entry"]["organism"]["name"]. '</i></br>';
			}
			$query = 'SELECT homologues,homology_method FROM Slims WHERE id = '.$id;
			$result = $connect->query($query);
			$row = $result->fetch_assoc();
			echo '<b>Number of homologues : </b>' .$row['homologues'];
			$query_al = 'SELECT refseq FROM uniprot_refseq WHERE uniprot = (SELECT uniprot FROM Slims where id = ' .$id. ')';
			$result_al = $connect->query($query_al);
			$row_al = $result_al->fetch_assoc();
			//Create the download link and button
			$file_align = 'align/seq_UNK__' .$row_al['refseq']. '.fasta';
			//$file_tsv = 'align/seq_UNK__' .$row_al['refseq']. '.tsv';
			$file = file_get_contents($file_align);
			if (trim($file) == true) {
				echo '&nbsp;&nbsp;<a href="' .$file_align. '" download="' .$file_align. '"><button type="submit">Download MSA</button></a>';
				echo '&nbsp;&nbsp;<a href="msa.php?seq=' . $row_al['refseq'] . '"><button type="submit">Visualize MSA</button></a>';
			}
			echo '</br>';
			echo '<b>Homology method : </b>' .$row['homology_method']. '</br>';
			//print_r($array["entry"]);
			$len_prot = strlen($array["entry"]["sequence"]);
			echo '<b>Length : </b>' .strlen($array["entry"]["sequence"]). '</br>';
			if (array_key_exists('comment', $array["entry"])) {
				echo '<b>Function : </b>' .$array["entry"]["comment"]["0"]["text"]. '</br>';
			}
			echo '</fieldset>';
			
			//Extract informations about the motif
			$query = 'SELECT id,uniprot,homologues,slims,motifs,start_pos,end_pos,aa_properties_conservation,positional_conservation,disorder,accessibility,probability,origin,datetimeActu,overall_conservation,taxonomy FROM Slims WHERE id = '.$id;
			$result = $connect->query($query);
			$row = $result->fetch_assoc();
			echo '<fieldset id="bigboxes"> <legend id="smallboxes">About the motif</legend>';
			echo '<b>EMD Id : </b>' .$row['id']. '</br>';
			echo '<b>SLiM name : </b>' .$row['slims']. '</br>';
			$motif_name = (string) $row['slims'];
			echo '<b>Sequence : </b>' .$row['motifs']. '</br>';
			if (substr( $row['slims'], 0, 4 ) !== "XLMS" and substr( $row['slims'], 0, 3 ) !== "PSP"){
				$query_regex = 'SELECT * FROM elm_classes WHERE slims = "'.$row['slims'].'"';
				$result_regex = $connect->query($query_regex);
				$row_regex = $result_regex->fetch_assoc();
				echo '<b>Regular expression : </b>' . $row_regex['regex'] . '</br>';
			}
			echo '<b>Position : </b>' .$row['start_pos']. '-' .$row['end_pos']. '</br>';
			$str_motf = (int) $row['start_pos'];
			$ed_motf = (int) $row['end_pos'];
			if (substr( $row['taxonomy'], 0, 6 ) !== "Living" ){
				$tax = explode(" ",$row['taxonomy']);
				if (count($tax) > 2) {
					$taxid = ltrim(rtrim($tax[2] ,")"),"(");
					echo '<b>Taxonomy (taxid) : </b>' .$tax[0]. ' ' . $tax[1] . ' (<a target="_blank" rel="noopener noreferrer" href="https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=' . $taxid. '">'.$taxid. '</a>)</br>';
				} else {
					$taxid = ltrim(rtrim($tax[1] ,")"),"(");
					echo '<b>Taxonomy (taxid) : </b>' .$tax[0] . ' (<a target="_blank" rel="noopener noreferrer" href="https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=' . $taxid. '">'.$taxid. '</a>)</br>';
				}
			} else {
				$tax = explode(" ",$row['taxonomy']);
				echo '<b>Taxonomy (taxid) : </b>' .$tax[0]. ' ' . $tax[1] .'</br>';
			}
			
			//echo '<b>Number of homologues : </b>' .$row['homologues_with_motif']. '</br>';
			echo '<b>Origin of the motif : </b>' .$row['origin']. '</br>';
			if (substr( $row['slims'], 0, 4 ) !== "XLMS" and substr( $row['slims'], 0, 3 ) !== "PSP"){
				$query_desc = 'SELECT description FROM elm_classes WHERE slims = "'.$motif_name . '"';
				$result_desc = $connect->query($query_desc);
				$row_desc = $result_desc->fetch_assoc();
				echo '<b> ELM motif description : </b>' . $row_desc['description'] . '</br>';
				echo '<b>ELM link : </b><a target="_blank" rel="noopener noreferrer" href="http://elm.eu.org/instances/' . $row['slims'] . '/'. $row['uniprot'] . '/' . 
						$row['start_pos'] . '"> elm.eu.org/instances/' . $row['slims'] . '/'. $row['uniprot'] . '/' . 
						$row['start_pos'] . '</a></br>';
			}
			echo '<b>Date of creation : </b>' .$row['datetimeActu']. '</br>';
			echo '<fieldset id="miniboxes"> <legend id="smallminiboxes">Scores</legend>';
			
			if ($row['positional_conservation'] <0.25){
				$pc_cat = 'Remotely conserved';
			} elseif ((0.25 <= $row['positional_conservation']) && ($row['positional_conservation'] < 0.5)) {
				$pc_cat = 'Weakly conserved';
			} elseif ((0.5 <= $row['positional_conservation']) && ($row['positional_conservation'] < 0.75)) {
				$pc_cat = 'Conserved';
			} else {
				$pc_cat = 'Highly conserved';
			}
			
			if ($row['overall_conservation'] <0.25){
				$oc_cat = 'Remotely conserved';
			} elseif ((0.25 <= $row['overall_conservation']) && ($row['overall_conservation'] < 0.5)) {
				$oc_cat = 'Weakly conserved';
			} elseif ((0.5 <= $row['overall_conservation']) && ($row['overall_conservation'] < 0.75)) {
				$oc_cat = 'Conserved';
			} else {
				$oc_cat = 'Highly conserved';
			}
			
			if ($row['aa_properties_conservation'] <0.25){
				$ac_cat = 'Remotely conserved';
			} elseif ((0.25 <= $row['aa_properties_conservation']) && ($row['aa_properties_conservation'] < 0.5)) {
				$ac_cat = 'Weakly conserved';
			} elseif ((0.5 <= $row['aa_properties_conservation']) && ($row['aa_properties_conservation'] < 0.75)) {
				$ac_cat = 'Conserved';
			} else {
				$ac_cat = 'Highly conserved';
			}
			
			if ($row['disorder'] <0.25){
				$d_cat = 'Structured';
			} elseif ((0.25 <= $row['disorder']) && ($row['disorder'] < 0.5)) {
				$d_cat = 'Partially structured';
			} elseif ((0.5 <= $row['disorder']) && ($row['disorder'] < 0.75)) {
				$d_cat = 'Partially disordered';
			} else {
				$d_cat = 'Disordered';
			}
			
			if ($row['accessibility'] <3){
				$sa_cat = 'Buried';
			} elseif ((03 <= $row['accessibility']) && ($row['accessibility'] < 6)) {
				$sa_cat = 'Intermediate';
			} else {
				$sa_cat = 'Exposed';
			}
			
			echo '<b>Positional conservation : </b>' .round($row['positional_conservation'],2). ' (' . $pc_cat .')</br>';
			echo '<b>Disorder : </b>' .$row['disorder']. ' (' . $d_cat .')</br>';
			echo '<b>Overall conservation : </b>' .round($row['overall_conservation'],2). ' (' . $oc_cat .')</br>';
			echo '<b>AA properties conservation : </b>' .round($row['aa_properties_conservation'],2). ' (' . $ac_cat .')</br>';
			echo '<b>Surface accessibility : </b>' .$row['accessibility']. ' (' . $sa_cat .')</br>';
			if ($row['probability'] != NULL and $row['probability'] != 0) {
				echo '<b>Probability : </b>' .$row['probability']. '</br>';
			}
			echo '</fieldset>';
			
			//Interacting domains
			
			$query = 'SELECT * FROM motif_domain where slims=(SELECT slims FROM Slims where id = ' .$id. ')';
			$result = $connect->query($query);
			$row_cnt = $result->num_rows;
			if ($row_cnt !== 0) {
				echo '<fieldset id="miniboxes"> <legend id="smallminiboxes">Interacts with</legend>';
				echo ' <b>Domains : </b><ul>';
				while ($row = $result->fetch_assoc()) {
					if (substr( $row['domain_id'], 0, 2) === "PF") {
						echo '<li><a target="_blank" rel="noopener noreferrer" href="https://pfam.xfam.org/family/' .$row['domain_id']. '">' .$row['domain_id']. '</a> : ' .$row['description']. '</li>';
					}
					elseif (substr( $row['domain_id'], 0, 3) === "IPR") {
						echo '<li><a target="_blank" rel="noopener noreferrer" href="https://www.ebi.ac.uk/interpro/entry/InterPro/' .$row['domain_id']. '">' .$row['domain_id']. '</a> : ' .$row['description']. '</li>';
					}
					elseif (substr( $row['domain_id'], 0, 2) === "SM") {
						echo '<li><a target="_blank" rel="noopener noreferrer" href="http://smart.embl-heidelberg.de/smart/do_annotation.pl?DOMAIN=' .$row['domain_id']. '">' .$row['domain_id']. '</a> : ' .$row['description']. '</li>';
					}
				}
				echo '</ul>';
				echo '</fieldset>';
			}
			
			$query = 'SELECT * FROM motif_interactions where slims_id1= ' .$id;
			$result = $connect->query($query);
			$row_cnt = $result->num_rows;
			if ($row_cnt !== 0) {
				echo '<fieldset id="miniboxes"> <legend id="smallminiboxes">Interacts with</legend>';
				echo ' <b>Motifs : </b><ul>';
				while ($row = $result->fetch_assoc()) {
					echo '<li><a href="slim.php?id=' .$row['slims_id2']. '">' .$row['slims_id2']. '</a> : ' .$row['slims2']. '</li>';
				}
				echo '</ul>';
				echo '</fieldset>';
			}
			
			
			
			//Extract Go terms
			$query = 'SELECT * FROM goterms where slims=(SELECT slims FROM Slims where id = ' .$id. ')';
			$result = $connect->query($query);
			if(mysqli_num_rows($result)!==0){
				echo '<fieldset id="miniboxes"> <legend id="smallminiboxes">GO terms</legend><ul>';
				while ($row = $result->fetch_assoc()) {
					echo "<li><a target='_blank' rel='noopener noreferrer' href='http://amigo.geneontology.org/amigo/term/" . $row['go_id'] . "'>".$row['go_id']."</a> : ".$row['go_desc']. "</li>";
				}
				echo '</ul></fieldset>';
			}
			
			//Extract overlapping domains for XLMS motifs
			$query = 'SELECT * FROM xlms_overlaped_regions where uniprot="' .$unip. '" and slims="'.$motif_name.'" and start_pos='.$str_motf.' and end_pos='. $ed_motf;
			$result = $connect->query($query);
			if(mysqli_num_rows($result)!==0){
				echo '<fieldset id="miniboxes"> <legend id="smallminiboxes">Overlapping in '.$refseq_id.'</legend>';
			}
			
			$query2 = 'SELECT * FROM xlms_overlaped_regions where uniprot="' .$unip. '" and slims="'.$motif_name.'" and start_pos='.$str_motf.' and end_pos='. $ed_motf. ' and type_region="Region"';
			$result2 = $connect->query($query2);
			if(mysqli_num_rows($result2)!==0){
				echo ' <b>Regions : </b><ul>';
				while ($row = $result2->fetch_assoc()) {
					echo '<li>'.$row['description_region'].' ('.$row['start_region']. '-'. $row['end_region'] .')</li>';
				}
				echo '</ul>';
			}
			
			$query3 = 'SELECT * FROM xlms_overlaped_regions where uniprot="' .$unip. '" and slims="'.$motif_name.'" and start_pos='.$str_motf.' and end_pos='. $ed_motf. ' and type_region="Site"';
			$result3 = $connect->query($query3);
			if(mysqli_num_rows($result3)!==0){
				echo ' <b>Sites : </b><ul>';
				while ($row = $result3->fetch_assoc()) {
					if ($row['start_region'] !== $row['end_region']) {
						echo '<li>'.$row['description_region'].' ('.$row['start_region']. '-'. $row['end_region'] .')</li>';
					} else {
						echo '<li>'.$row['description_region'].' ('.$row['start_region'].')</li>';
					}
				}
				echo '</ul>';
			}
			
			if(mysqli_num_rows($result)!==0){
				echo '</fieldset>';
			}
			
			echo '</fieldset>';
			
			//Extract all the motifs found in this protein
			$query = 'SELECT * FROM Slims WHERE uniprot = (SELECT uniprot FROM Slims where id = ' .$id. ') ORDER BY start_pos';
			$result = $connect->query($query);
			echo '<fieldset id="bigboxes"> <legend id="smallboxes">All motifs found in ' .$unip. '</legend>';
			
			//Representation of the protein with its motifs
			echo '<canvas id="myCanvas" width="1540" height="110">
					Your browser does not support the HTML5 canvas tag.</canvas><br>
					<div> <b>Motif selected : </b> <span id="tooltiptext" ></span></div>
					
				<script>
					var c = document.getElementById("myCanvas");
					var ctx = c.getContext("2d");
					ctx.beginPath();
					ctx.rect(20, 20,'. $len_prot*(1500/$len_prot) .', 80);
					ctx.rect(20, 105,'. $len_prot*(1500/$len_prot) .', 1);
					ctx.stroke();
				</script> ';
			for($i = 20; $i < $len_prot*(1500/$len_prot)+20; $i+=10*(1500/$len_prot)) {
				echo '<script>
					var c = document.getElementById("myCanvas");
					var ctx = c.getContext("2d");
					ctx.beginPath();
					ctx.rect('.$i.', 105, 1, 4);
					ctx.stroke();
					var rects = [];
					var rects2 = [];
				</script> ';
			}
			echo '<br>';
			while ($row = $result->fetch_assoc()) {
				$strt = $row['start_pos']*(1500/$len_prot) +20;
				$ed = $row['end_pos']*(1500/$len_prot) +20 - $strt ;
				//$id = $row['id'];
				echo '<script>
					
					var c = document.getElementById("myCanvas");
					var ctx = c.getContext("2d");
					rects2 = rects2.concat({x: '.$strt.', y: 20, w: '. $ed .', h: 80, tip: "<a href=\"slim.php?id=' . $row['id'] . '\">'.$row['id'].' </a>\r\n'.$row['slims'].'"});
					ctx.beginPath();

					ctx.fillStyle = "#9d9999";
					ctx.fillRect('.$strt . ', 20,'. $ed .', 80);
					
					ctx.stroke();
				</script>';
			}
			echo '<script>
				var c = document.getElementById("myCanvas");
				var ctx = c.getContext("2d");
				c.onmousemove = function(e) {
					// important: correct mouse position:
					var rect = this.getBoundingClientRect(),
					x = e.clientX - rect.left,
					y = e.clientY - rect.top,
					i = 0,
					r;

					while (r = rects2[i++]) {
						// add a single rect to path:
						ctx.beginPath();
						ctx.rect(r.x, r.y, r.w, r.h);

						// check if we hover it, fill red, if not fill it blue
						ctx.fillStyle = ctx.isPointInPath(x, y) ? "#0099cc" : "#9d9999";
						var t = document.getElementById("tooltiptext");
						if (ctx.isPointInPath(x, y)){
							t.innerHTML = r.tip;
						}
						ctx.fill();
					}

				};
				
			</script>';
			//Table containing all the motifs of this protein
			$result = $connect->query($query);
			echo '<table id="table_motifs"><thead><tr>
				<th>EMD Id</th>
				<th>SLiM</th>
				<th>Motif</th>
				<th>Start</th>
				<th>End</th>
				<th>Positional conservation score</th>
				<th>Disorder score</th>
				<th>Surface accessibility score</th>
				<th>Overall conservation score</th>
				<th>AA properties conservation score</th>
				<th>Taxonomy (taxid)</th>
				</tr></thead>';
			while ($row = $result->fetch_assoc()) {
				//Create a link to these motifs
				echo "<tr><td><a href='slim.php?id=" . $row['id'] . "'>".$row['id']."</a></td><td>".$row['slims']."</td><td>".$row['motifs']."</td><td>".$row['start_pos']."</td><td>".$row['end_pos']."</td><td>".round($row['positional_conservation'],3)."</td><td>".round($row['disorder'],3)."</td><td>".round($row['accessibility'],3)."</td><td>".round($row['overall_conservation'],3)."</td><td>".round($row['aa_properties_conservation'],3)."</td><td>".$row['taxonomy']."</td></tr>";
			}
			echo '</table></fieldset>';
			
			//Structures if available for the motif
			$query = 'SELECT * FROM motifs_structures WHERE
					uniprot = (SELECT uniprot FROM Slims WHERE id = ' .$id. ')
					AND start_pos = (SELECT start_pos FROM Slims WHERE id = ' .$id. ')
					AND end_pos = (SELECT end_pos FROM Slims WHERE id = ' .$id. ')
					AND motif_sequence = (SELECT motifs FROM Slims WHERE id = ' .$id. ')';
			
			$result = $connect->query($query);
			if(mysqli_num_rows($result)!==0)
			{
				echo '<fieldset id="alignboxes"> <legend id="smallboxes">Structure representations</legend>';
				while ($row = $result->fetch_assoc()) {
					$png_name = "png/".$row['uniprot'] . "_" .$row['PDB_id'].  "_" .$row['start_pos'] . "_" .$row['end_pos'].  ".png";
					$pdb_name = "Motifs_PDB/".$row['uniprot'] . "_" .$row['PDB_id'].  "_" .$row['start_pos'] . "_" .$row['end_pos'].".pdb";
					$pdb_file = $row['uniprot'] . "_" .$row['PDB_id'].  "_" .$row['start_pos'] . "_" .$row['end_pos'].".pdb";
					list($width_orig, $height_orig) = getimagesize($png_name);
					$width = intval($width_orig) ;
					$height = intval($height_orig) ;
					echo "<b>Corresponding structure : </b> <i>".$row['structure_sequence']."</i> - <a href='pdb_motif.php?id=".$row['uniprot'] . "_" .$row['PDB_id'].  "_" .$row['start_pos'] . "_" .$row['end_pos'].".pdb' >" . $pdb_file. "</a><br><img src='".$png_name."' width=" .$width. " height=" .$height. "><br>";
				}
				echo '</fieldset>';
			}
			
			//Create the box for the multiple sequence alignment
			//Extract the refseq Id
			/*$query = 'SELECT refseq FROM uniprot_refseq WHERE uniprot = (SELECT uniprot FROM Slims where id = ' .$id. ')';
			$result = $connect->query($query);
			$row = $result->fetch_assoc();
			//Create the download link and button
			$file_align = 'align/seq_UNK__' .$row['refseq']. '.fasta';
			$file_tsv = 'align/seq_UNK__' .$row['refseq']. '.tsv';
			$file = file_get_contents($file_tsv);
			if (trim($file) == true) {
				echo '<fieldset id="alignboxes"> <legend id="smallboxes">Multiple sequence alignment</legend>';
				//echo '<p><form method="get" action="' .$file_align. '"><button type="submit">Download fasta</button></form></p>';
				//echo $file_align;
				//echo '<p><a href="' .$file_align. '" download="' .$file_align. '"><button type="submit">Download fasta</button></a></p>';
				//Create the space for the alignment output
				echo '<div id="MSA_field"></div>' ;
				echo '<pre style="display: none" id="fasta-file">' .$file. '</pre>';
				echo '</fieldset>';
			}*/
		?>
		<!--<script>
			function csv_To_Array(str, delimiter = "\t") {
		      const header_cols = ["id","sequence"];
		      const row_data = str.slice(str.indexOf("\n") + 1).split("\n");
		      const arr = row_data.map(function (row) {
		        const values = row.split(delimiter);
		        const el = header_cols.reduce(function (object, header, index) {
		          object[header] = values[index];
		          return object;
		        }, {});
		        return el;
		      });
		
		      // return the array
		      return arr;
		    }
				const reader = new FileReader();
				const text = document.getElementById("fasta-file").innerText;
				const data = csv_To_Array(text);
				//const data = $.csv.toArrays(text, {
//  delimiter: "\t", // Sets a custom field separator character
//});
				data.pop();
				data.pop();
				//document.getElementById("MSA_field").innerHTML = JSON.stringify(data, null, 2);
				var options = Array();
				options.consensus = true;
				options.scrollX = "100px";
				options.scrollY = "100px";
				options.colourScheme = "clustal";
				printJSAV("MSA_field",data,options);
		</script>-->
		
		
    </p>
    
    <!-- Include the foot -->
    <?php include("foot.php"); ?>
    
    </body>

</html>




