CREATE DATABASE slimDatabase CHARACTER SET 'utf8';

USE slimDatabase;


CREATE TABLE Slims (
	id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
	uniprot varchar(20) NOT NULL,
	homologues SMALLINT NOT NULL,
	homology_method varchar(20) NOT NULL,
	slims varchar(40),
	motifs varchar(100) NOT NULL,
	start_pos SMALLINT NOT NULL,
	end_pos SMALLINT NOT NULL,
	homologues_with_motif SMALLINT NOT NULL,
	positional_conservation DOUBLE,
	disorder DOUBLE,
	accessibility DOUBLE,
	overall_conservation DOUBLE  PRECISION(4,3),
	taxonomy varchar(100),
	probability DOUBLE,
	origin VARCHAR(1000),
	datetimeActu TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY (id)
	)
	ENGINE=INNODB;

LOAD DATA LOCAL INFILE '~/etnadb/db_content/ELM_without_unclassified.tsv'
INTO TABLE Slims
FIELDS TERMINATED BY '\t'
LINES TERMINATED BY '\n'
IGNORE 1 LINES
(uniprot,slims,start_pos,end_pos,motifs,positional_conservation,disorder,accessibility,homologues_with_motif,homologues,overall_conservation,probability,taxonomy,origin,homology_method);

LOAD DATA LOCAL INFILE '~/etnadb/db_content/XLMS_human_without_unclassified.tsv'
INTO TABLE Slims
FIELDS TERMINATED BY '\t'
LINES TERMINATED BY '\n'
IGNORE 1 LINES
(uniprot,slims,start_pos,end_pos,motifs,positional_conservation,disorder,accessibility,homologues_with_motif,homologues,overall_conservation,taxonomy,origin,homology_method);

LOAD DATA LOCAL INFILE '~/etnadb/db_content/PSP_OGLC_without_unclassified.tsv'
INTO TABLE Slims
FIELDS TERMINATED BY '\t'
LINES TERMINATED BY '\n'
IGNORE 1 LINES
(uniprot,slims,start_pos,end_pos,motifs,positional_conservation,disorder,accessibility,homologues_with_motif,homologues,overall_conservation,taxonomy,origin,homology_method);


CREATE TABLE motif_domain (
	id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
	slims varchar(40) NOT NULL,
	domain_id varchar(50) NOT NULL,
	description varchar(100),
	PRIMARY KEY (id)
	)
	ENGINE=INNODB;

LOAD DATA LOCAL INFILE '~/etnadb/db_content/elm_interaction_domains.tsv'
INTO TABLE motif_domain
FIELDS TERMINATED BY '\t'
LINES TERMINATED BY '\n'
IGNORE 1 LINES
(slims,domain_id,description);


CREATE TABLE goterms (
	id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
	slims varchar(40) NOT NULL,
	go_id varchar(40) NOT NULL,
	go_desc varchar(200),
	PRIMARY KEY (id)
	)
	ENGINE=INNODB;

LOAD DATA LOCAL INFILE '~/etnadb/db_content/elm_goterms.tsv'
INTO TABLE goterms
FIELDS TERMINATED BY '\t'
LINES TERMINATED BY '\n'
IGNORE 1 LINES
(slims,go_id,go_desc);


CREATE TABLE uniprot_refseq (
	id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
	uniprot varchar(20) NOT NULL,
	refseq varchar(30) NOT NULL,
	PRIMARY KEY (id)
	)
	ENGINE=INNODB;

LOAD DATA LOCAL INFILE '~/etnadb/db_content/ELM_without_unclassified_refuni_uniq.tsv'
INTO TABLE uniprot_refseq
FIELDS TERMINATED BY '\t'
LINES TERMINATED BY '\n'
IGNORE 1 LINES
(refseq,uniprot);

LOAD DATA LOCAL INFILE '~/etnadb/db_content/XLMS_human_without_unclassified_refuni_uniq.tsv'
INTO TABLE uniprot_refseq
FIELDS TERMINATED BY '\t'
LINES TERMINATED BY '\n'
IGNORE 1 LINES
(refseq,uniprot);

LOAD DATA LOCAL INFILE '~/etnadb/db_content/PSP_OGLC_without_unclassified_refuni_uniq.tsv'
INTO TABLE uniprot_refseq
FIELDS TERMINATED BY '\t'
LINES TERMINATED BY '\n'
IGNORE 1 LINES
(refseq,uniprot);

CREATE TABLE elm_classes (
	id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
	slims varchar(100) NOT NULL,
	description varchar(500) NOT NULL,
	regex varchar(200) NOT NULL,
	PRIMARY KEY (id)
	)
	ENGINE=INNODB;

LOAD DATA LOCAL INFILE '~/etnadb/db_content/elm_classes.tsv'
INTO TABLE elm_classes
FIELDS TERMINATED BY '\t'
LINES TERMINATED BY '\n'
IGNORE 1 LINES
(slims,description,regex);

CREATE TABLE xlms_overlaped_regions (
	id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
	accession varchar(100) NOT NULL,
	uniprot varchar(100) NOT NULL,
	slims varchar(100) NOT NULL,
	start_pos SMALLINT NOT NULL,
	end_pos SMALLINT NOT NULL,
	description_region varchar(500) NOT NULL,
	start_region SMALLINT NOT NULL,
	end_region SMALLINT NOT NULL,
	type_region varchar(100) NOT NULL,
	PRIMARY KEY (id)
	)
	ENGINE=INNODB;

LOAD DATA LOCAL INFILE '~/etnadb/db_content/xlms_human_1_overlaping_regions.tsv'
INTO TABLE xlms_overlaped_regions
FIELDS TERMINATED BY '\t'
LINES TERMINATED BY '\n'
IGNORE 1 LINES
(accession,uniprot,slims,start_pos,end_pos,description_region,start_region,end_region,type_region);

LOAD DATA LOCAL INFILE '~/etnadb/db_content/xlms_human_2_overlaping_regions.tsv'
INTO TABLE xlms_overlaped_regions
FIELDS TERMINATED BY '\t'
LINES TERMINATED BY '\n'
IGNORE 1 LINES
(accession,uniprot,slims,start_pos,end_pos,description_region,start_region,end_region,type_region);

LOAD DATA LOCAL INFILE '~/etnadb/db_content/xlms_human_3_overlaping_regions.tsv'
INTO TABLE xlms_overlaped_regions
FIELDS TERMINATED BY '\t'
LINES TERMINATED BY '\n'
IGNORE 1 LINES
(accession,uniprot,slims,start_pos,end_pos,description_region,start_region,end_region,type_region);

CREATE TABLE motifs_structures (
	id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
	uniprot varchar(20) NOT NULL,
	accession varchar(100) NOT NULL,
	PDB_id varchar(20) NOT NULL,
	start_pos SMALLINT NOT NULL,
	end_pos SMALLINT NOT NULL,
	start_pdb SMALLINT NOT NULL,
	end_pdb SMALLINT NOT NULL,
	chain_pdb varchar(100) NOT NULL,
	motif_sequence varchar(100) NOT NULL,
	structure_sequence varchar(100) NOT NULL,
	PRIMARY KEY (id)
	)
	ENGINE=INNODB;

LOAD DATA LOCAL INFILE '~/etnadb/db_content/STRUCTURE_ELM_compile_1_2_loc.tsv'
INTO TABLE motifs_structures
FIELDS TERMINATED BY '\t'
LINES TERMINATED BY '\n'
IGNORE 1 LINES
(uniprot,accession,motif_sequence,structure_sequence,start_pos,end_pos,PDB_id,start_pdb,end_pdb,chain_pdb);

LOAD DATA LOCAL INFILE '~/etnadb/db_content/STRUCTURE_ELM_compile_3_4_loc.tsv'
INTO TABLE motifs_structures
FIELDS TERMINATED BY '\t'
LINES TERMINATED BY '\n'
IGNORE 1 LINES
(uniprot,accession,motif_sequence,structure_sequence,start_pos,end_pos,PDB_id,start_pdb,end_pdb,chain_pdb);

LOAD DATA LOCAL INFILE '~/etnadb/db_content/STRUCTURE_ELM_compile_5_6_loc.tsv'
INTO TABLE motifs_structures
FIELDS TERMINATED BY '\t'
LINES TERMINATED BY '\n'
IGNORE 1 LINES
(uniprot,accession,motif_sequence,structure_sequence,start_pos,end_pos,PDB_id,start_pdb,end_pdb,chain_pdb);

LOAD DATA LOCAL INFILE '~/etnadb/db_content/STRUCTURE_ELM_compile_7_kmd_loc.tsv'
INTO TABLE motifs_structures
FIELDS TERMINATED BY '\t'
LINES TERMINATED BY '\n'
IGNORE 1 LINES
(uniprot,accession,motif_sequence,structure_sequence,start_pos,end_pos,PDB_id,start_pdb,end_pdb,chain_pdb);

LOAD DATA LOCAL INFILE '~/etnadb/db_content/STRUCTURE_XLMS_human_1_loc.tsv'
INTO TABLE motifs_structures
FIELDS TERMINATED BY '\t'
LINES TERMINATED BY '\n'
IGNORE 1 LINES
(uniprot,accession,motif_sequence,structure_sequence,start_pos,end_pos,PDB_id,start_pdb,end_pdb,chain_pdb);

LOAD DATA LOCAL INFILE '~/etnadb/db_content/STRUCTURE_XLMS_human_2.tsv'
INTO TABLE motifs_structures
FIELDS TERMINATED BY '\t'
LINES TERMINATED BY '\n'
IGNORE 1 LINES
(uniprot,accession,motif_sequence,structure_sequence,start_pos,end_pos,PDB_id,start_pdb,end_pdb,chain_pdb);

LOAD DATA LOCAL INFILE '~/etnadb/db_content/STRUCTURE_PSP_OGLC_1.tsv'
INTO TABLE motifs_structures
FIELDS TERMINATED BY '\t'
LINES TERMINATED BY '\n'
IGNORE 1 LINES
(uniprot,accession,motif_sequence,structure_sequence,start_pos,end_pos,PDB_id,start_pdb,end_pdb,chain_pdb);

LOAD DATA LOCAL INFILE '~/etnadb/db_content/STRUCTURE_PSP_OGLC_2.tsv'
INTO TABLE motifs_structures
FIELDS TERMINATED BY '\t'
LINES TERMINATED BY '\n'
IGNORE 1 LINES
(uniprot,accession,motif_sequence,structure_sequence,start_pos,end_pos,PDB_id,start_pdb,end_pdb,chain_pdb);

LOAD DATA LOCAL INFILE '~/etnadb/db_content/STRUCTURE_PSP_OGLC_3.tsv'
INTO TABLE motifs_structures
FIELDS TERMINATED BY '\t'
LINES TERMINATED BY '\n'
IGNORE 1 LINES
(uniprot,accession,motif_sequence,structure_sequence,start_pos,end_pos,PDB_id,start_pdb,end_pdb,chain_pdb);

LOAD DATA LOCAL INFILE '~/etnadb/db_content/STRUCTURE_XLMS_human_3.tsv'
INTO TABLE motifs_structures
FIELDS TERMINATED BY '\t'
LINES TERMINATED BY '\n'
IGNORE 1 LINES
(uniprot,accession,motif_sequence,structure_sequence,start_pos,end_pos,PDB_id,start_pdb,end_pdb,chain_pdb);

CREATE TABLE motif_motif (
	id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
	slims_id1 SMALLINT UNSIGNED,
	protein1 varchar(40) NOT NULL,
	motif1 varchar(100) NOT NULL,
	slims_id2 SMALLINT UNSIGNED,
	protein2 varchar(40) NOT NULL,
	motifs2 varchar(100) NOT NULL,
	PRIMARY KEY (id)
	)
	ENGINE=INNODB;

LOAD DATA LOCAL INFILE '~/etnadb/db_content/Ecoli_XLMS_unique_inter.tsv'
INTO TABLE motif_motif
FIELDS TERMINATED BY '\t'
LINES TERMINATED BY '\n'
IGNORE 1 LINES
(protein1,motif1,protein2,motifs2);

LOAD DATA LOCAL INFILE '~/etnadb/db_content/HeLa_unique_inter.csv'
INTO TABLE motif_motif
FIELDS TERMINATED BY '\t'
LINES TERMINATED BY '\n'
IGNORE 1 LINES
(protein1,motif1,protein2,motifs2);

CREATE TABLE motif_int (
	id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
	PRIMARY KEY (id))
	ENGINE=INNODB
	SELECT Slims.id slims_id1,slims slims1,motif1,protein1,motifs2,protein2 
	FROM Slims,motif_motif 
	WHERE motifs = motif1 AND uniprot = protein1 
	ORDER BY motif_motif.id;
	
CREATE TABLE motif_interactions (
	id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
	PRIMARY KEY (id))
	ENGINE=INNODB
	SELECT slims_id1,slims1,motif1,protein1,Slims.id slims_id2,slims slims2,motifs2,protein2 
	FROM Slims,motif_int 
	WHERE motifs = motifs2 AND uniprot = protein2
	ORDER BY motif_int.id;

DROP TABLE motif_motif;
DROP TABLE motif_int ;






LOAD DATA LOCAL INFILE '~/etnadb/db_content/PSP_Acety_part1_without_unclassified.tsv'
INTO TABLE Slims
FIELDS TERMINATED BY '\t'
LINES TERMINATED BY '\n'
IGNORE 1 LINES
(uniprot,slims,start_pos,end_pos,motifs,positional_conservation,disorder,accessibility,homologues_with_motif,homologues,overall_conservation,taxonomy,origin,homology_method);

LOAD DATA LOCAL INFILE '~/etnadb/db_content/PSP_Acety_part1_without_unclassified_refuni_uniq.tsv'
INTO TABLE uniprot_refseq
FIELDS TERMINATED BY '\t'
LINES TERMINATED BY '\n'
IGNORE 1 LINES
(refseq,uniprot);

LOAD DATA LOCAL INFILE '~/etnadb/db_content/PSP_Methy_part1_without_unclassified.tsv'
INTO TABLE Slims
FIELDS TERMINATED BY '\t'
LINES TERMINATED BY '\n'
IGNORE 1 LINES
(uniprot,slims,start_pos,end_pos,motifs,positional_conservation,disorder,accessibility,homologues_with_motif,homologues,overall_conservation,taxonomy,origin,homology_method);

LOAD DATA LOCAL INFILE '~/etnadb/db_content/PSP_Methy_part1_without_unclassified_refuni_uniq.tsv'
INTO TABLE uniprot_refseq
FIELDS TERMINATED BY '\t'
LINES TERMINATED BY '\n'
IGNORE 1 LINES
(refseq,uniprot);

LOAD DATA LOCAL INFILE '~/etnadb/db_content/PSP_OGal_part1_without_unclassified.tsv'
INTO TABLE Slims
FIELDS TERMINATED BY '\t'
LINES TERMINATED BY '\n'
IGNORE 1 LINES
(uniprot,slims,start_pos,end_pos,motifs,positional_conservation,disorder,accessibility,homologues_with_motif,homologues,overall_conservation,taxonomy,origin,homology_method);

LOAD DATA LOCAL INFILE '~/etnadb/db_content/PSP_OGal_part1_without_unclassified_refuni_uniq.tsv'
INTO TABLE uniprot_refseq
FIELDS TERMINATED BY '\t'
LINES TERMINATED BY '\n'
IGNORE 1 LINES
(refseq,uniprot);

LOAD DATA LOCAL INFILE '~/etnadb/db_content/STRUCTURE_PSP_Acetylation_part1.tsv'
INTO TABLE motifs_structures
FIELDS TERMINATED BY '\t'
LINES TERMINATED BY '\n'
IGNORE 1 LINES
(uniprot,accession,motif_sequence,structure_sequence,start_pos,end_pos,PDB_id,start_pdb,end_pdb,chain_pdb);

LOAD DATA LOCAL INFILE '~/etnadb/db_content/STRUCTURE_PSP_Methylation_part1.tsv'
INTO TABLE motifs_structures
FIELDS TERMINATED BY '\t'
LINES TERMINATED BY '\n'
IGNORE 1 LINES
(uniprot,accession,motif_sequence,structure_sequence,start_pos,end_pos,PDB_id,start_pdb,end_pdb,chain_pdb);

LOAD DATA LOCAL INFILE '~/etnadb/db_content/STRUCTURE_PSP_OGalNAc_part1.tsv'
INTO TABLE motifs_structures
FIELDS TERMINATED BY '\t'
LINES TERMINATED BY '\n'
IGNORE 1 LINES
(uniprot,accession,motif_sequence,structure_sequence,start_pos,end_pos,PDB_id,start_pdb,end_pdb,chain_pdb);
