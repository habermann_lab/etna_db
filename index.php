<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>evo-MOTiF database</title>
        <link rel="stylesheet"  href="style.css">
        <!-- Make references as a numeral list -->
        <style>
			ul.ref {
				list-style: number;
				}
        </style>
    </head>
 
    <body id="first_page">
 
	<!-- Include the header -->
    <?php include("head.php"); ?>
    
    <!-- Include the tabs -->
    <?php include("menus.php"); ?>
    
    <!-- Main page -->
    <div id="corps">
        <h2>Welcome !</h2>
        <p>evo-MOTiF database aims at being a resource for informations about conservation and specificity of motifs in 
            a structural context.
        </p>
        <p>
			SLiMs are short sequences found in proteins, represented by regular expressions, which are known 
			to interact with specific domains. They particularly occur in disordered regions but seem 
			strangely to be relatively conserved 
			[<a href="contact.php#references">1</a>]. Their small length, 
			lacking of tertiray structure and flexibility (represented by the regular expressions) make 
			them easier to evolve and develop <I>de novo</I> from exisiting peptides.
		</p>
		<p>
			In this database you could find motifs which where discovered by different methods and 
			their scores of conservation, disorder, surface accessibility and specificity. For more information 
			about these scores, please, take a look at the "About" section or at the SlimProb manual [<a href="contact.php#references">9</a>].
		</p>
		
        
    </div>
    
    <!-- Include the foot -->
    <?php include("foot.php"); ?>
    
    </body>
</html>
