<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>SLiMs-ETNA</title>
        <link rel="stylesheet"  href="style.css">
        <link rel="stylesheet"  href="slim.css">
        <script src="node_modules/ngl/dist/ngl.js"></script>
        <!-- Make references as a numeral list -->
        <style>
			ul.ref {
				list-style: number;
				}
        </style>
    </head>
 
    <body id="first_page">
 
	<!-- Include the header -->
    <?php include("head.php"); ?>
    <!-- Include the tabs -->
    <?php include("menus.php"); ?>
    
    <div id="corps">
		<br>
		<form method="GET">
			
			<?php
				// Extract the ELM ids to create a list
				//$connect = new mysqli('localhost', 'etnadb', 'pldbssr_2021','slimDatabase');
				$connect = new mysqli('localhost', 'root', 'dbssr1995','slimDatabase');
				$query = 'SELECT DISTINCT(ELMid) FROM elm_structures';
				$result = $connect->query($query);
				
				// Store the ELM ids in an array
				$arraySlims = array();
				foreach($result as $row) {
					array_push($arraySlims,$row['ELMid']);
				}
				$selected = '';
				
				// Go through the array to create the list
				echo '<select name="search" id="search">',"n";
				foreach($arraySlims as $id_array => $motifs_id){
					if($motifs_id === 'DEG_Kelch_Keap1_1'){
						$selected = ' selected="selected"';
					}
					echo "\t",'<option value="', $motifs_id ,'"', $selected ,'>', $motifs_id ,'</option>',"\n";
					$selected='';
				}
				echo '</select>',"\n";
			?>
			
			<input id="submit" type="submit" value="Search">
			
			<button id="download" type="button" ><a download="etna_structures.csv" href="#" onclick="return ExcellentExport.csv(this, 'table_motifs');" id="downloader">Export to csv</a></button>
			<?php

				//Connexion to the MySQL database
				//$connect = new mysqli('localhost', 'etnadb', 'pldbssr_2021','slimDatabase');
				$connect = new mysqli('localhost', 'root', 'dbssr1995','slimDatabase');
				if (isset($_GET["search"])) {
					$ELM_id = $_GET['search'];
					}
				else {
					$ELM_id = "DEG_Kelch_Keap1_1" ;
					}
					
				$query = 'SELECT * FROM elm_structures WHERE ELMid = "'.$ELM_id.'"';
				$result = $connect->query($query);
				echo "&emsp;" . $result->num_rows . " entries for <b>" .$ELM_id . "</b>.<br><br>";
				$c = 0;
				echo '<table id="table_motifs"><thead><tr>
					<th>ELM accession</th>
					<th>PDB</th>
					<th>UniProt</th>
					<th>Start</th>
					<th>End</th>
					<th>Motif sequence</th>
					<th>Structure</th>
					<th>2D representation</th>
					<th>3D representation</th>
					</tr></thead>';
				while ($row = $result->fetch_assoc()) {
					//Create a link to these motifs
					$png_name = "png/".$row['accession'] . "_" .$row['PDB_id'].".png";
					$pdb_name = "Motifs_PDB/".$row['accession'] . "_" .$row['PDB_id'].".pdb";
					list($width_orig, $height_orig) = getimagesize($png_name);
					$width = intval($width_orig/2) ;
					$height = intval($height_orig/2) ;
					$c = $c + 1;
					echo "<tr><td><a href='http://elm.eu.org/instances/" . $row['ELMid'] . "/". $row['uniprot'] . "/" . 
						$row['start_pos'] . "'>".$row['accession']."</a></td><td><a href='https://www.rcsb.org/structure/".
						$row['PDB_id']. "'>".$row['PDB_id']."</td><td>".$row['uniprot']."</td><td>".$row['start_pos']."</td><td>".
						$row['end_pos']."</td><td>".$row['motif_sequence']."</td><td>".$row['structure_sequence'].
						"</td><td> <img src='".$png_name."' width=" .$width. " height=" .$height. "></td>
						
						<td id='3Dstruct'>
							<script type='text/javascript'>
								document.addEventListener('DOMContentLoaded', function () {
									var stage = new NGL.Stage('viewport');
									stage.loadFile('".$pdb_name."', { ext: 'pdb' }).then(function (o) {
										o.addRepresentation('cartoon', {colorScheme: 'sstruc'});
										o.autoView();
									});
									stage.setSpin(true);
								});
							</script>
						</td></tr>";
				}
				echo '</table>';
			?>
			
		</form>
	</div>
    
    <!-- Include the foot -->
    <?php include("foot.php"); ?>
    
    </body>
</html>

