<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>evo-MOTiF database</title>
        <link rel="stylesheet"  href="style.css">
        <link rel="stylesheet"  href="slim.css">
        <!-- Make references as a numeral list and the pipeline as a dotted list-->
        
    </head>
 
    <body id="first_page">
 
	<!-- Include the header -->
    <?php include("head.php"); ?>
    
    <!-- Include the tabs -->
    <?php include("menus.php"); ?>
    
    <!-- Main page -->
    
    <div id="corps">
        <h2>Interpretation of the different scores</h2>
			The three first scores where calculated by SlimProb [<a href="contact.php#references">9</a>], associated with IUPred [<a href="contact.php#references">10</a>,<a href="contact.php#references">11</a>].
			<ol class="scores">
				<li><b>Positional conservation : </b>  0 remotely conserved &hArr; 1 conserved.</br>
				It corresponds to the proportion of residues (in the alignment position of the motif) corresponding to the (regular) expression.<br>
				&#9888; In the absence of homologues for the protein, the motif can be considered as not conserved. <br>
				For more information about this, please take a look at the SLiMProb manual <a target='_blank' rel='noopener noreferrer' href="https://github.com/slimsuite/SLiMSuite/blob/master/docs/manuals/SLiMProb%20Manual.pdf">here</a>.
				</li>
			
				<li><b>Disorder : </b> 0 structured &hArr; 1 disordered.
				</li>
			
				<li><b>Surface accessibility : </b> 0 buried &hArr; 9 exposed.
				</li>
				
				<li><b>Overall conservation : </b> 0 remotely conserved &hArr; 1 conserved.</br>
				The overall conservation is a rate between the proportion of homologues containig the motifs and the number 
				of homologues found for the protein. <br>
				A specific motif is a motif found in a small portion of the homologues of the protein. 
				Thus, the motif may be specific to a clade.
				</li>
				
				<li><b>Amino acid property conservation : </b> 0 remotely conserved &hArr; 1 conserved.</br>
				This score is calculated according to the similar properties of amino acids.
				</li>
				
				<li><b>Probability : </b> You would see that some motifs are associated with a probability score. It is given especially by 
				<a target='_blank' rel='noopener noreferrer' href="http://elm.eu.org/">elm.eu.org</a> [<a href="contact.php#references">2</a>] and correspond to the probability for 
				this motif to occur by chance.
				</li>
			</ol>
				For some statistics about evo-MOTiF database, please <input type="button" onclick="location.href='statistics.html';" value="Click here" />
				
		<h2>Structure codes</h2>
			We are using the DSSP codes to describe secondary structures for each residue [<a href="contact.php#references">16</a>].<br><br>
			<!-- Table for the structure representation meaning -->
			<table id="struct_code"><tr><th>Code</th><th>Structure</th></tr><tr><td>H</td><td>Alpha helix (4-12)</td></tr><tr><td>B</td><td>Isolated beta-bridge residue</td></tr><tr><td>E</td><td>Strand</td></tr><tr><td>G</td><td>3-10 helix</td></tr><tr><td>I</td><td>Pi helix</td></tr><tr><td>T</td><td>Turn</td></tr><tr><td>S</td><td>Bend</td></tr><tr><td>N</td><td>None</td></tr></table>
        
        <h2>Downloads</h2>
        
        <span style="vertical-align: middle;">The evo-MOTiF pipeline can be downloaded here : </span><a target='_blank' rel='noopener noreferrer' href="https://gitlab.com/habermann_lab/slims"><img src="pics/evologo.png" alt="pipeline" style="width:5%;height:5%;vertical-align: middle;" class="center"></a></br></br>
        
        <!-- Download link for all the alignment files -->
        <form method="get" action="align/alignments.zip"> 
			<button type="submit">Download all alignments</button>
			to download all multiple sequence alignments.
		</form>
        
        <!-- Download link for all the PDB files -->
        <form method="get" action="Motifs_PDB/all_PDB_motifs.zip">
			<button type="submit">Download all PDB files</button>
			to download all motifs PDB files.
		</form>
        
        <h2>Send us feebacks or your own motifs</h2> 
        If you want your motifs to be featured in the database, feel free to send them 
        to us. Please, respect the format of the database and send us your files in .csv or .tsv format 
        and use reliable sources.
        
        <h2>Citations</h2>
        
        If you are using evo-MOTiF or evo-MOTiF database, please cite :</br>
        </br>
        
        <h2 id="references"> References</h2>
			<ol class="ref">
				<li> Roey, K. V. et al. Short Linear Motifs: Ubiquitous and Functionally Diverse Protein Interaction Modules Directing Cell Regulation. <i>Chemical Reviews</i> <b>114</b>, 6733–6778 (2014). <a target='_blank' rel='noopener noreferrer' href="https://doi.org/10.1021/cr400585q">doi.org/10.1021/cr400585q</a> </li>
				<li> Kumar, M. et al. ELM - the eukaryotic linear motif resource in 2020. <i>Nucleic Acids Research</i> <b>48</b>, Issue D1, D296–D306 (2020). <a target='_blank' rel='noopener noreferrer' href="https://doi.org/10.1093/nar/gkz1030">doi.org/10.1093/nar/gkz1030</a></li>
				<li> Liu, F. et al. Optimized fragmentation schemes and data analysis strategies for proteome-wide cross-link identification. <i>Nature Communications</i> <b>8</b>, 15473, (2017). <a target='_blank' rel='noopener noreferrer' href="https://doi.org/10.1038/ncomms15473">doi.org/10.1038/ncomms15473</a> </li>
				<li> Hornbeck, P. V. et al. PhosphoSitePlus, 2014: mutations, PTMs and recalibrations. <i>Nucleic Acids Research</i> <b>43</b>, (2014). <a target='_blank' rel='noopener noreferrer' href="https://doi.org/10.1093/nar/gku1267">doi.org/10.1093/nar/gku1267</a></li>
				<li> Wolf, Y. I. & Koonin, E. V. A Tight Link between Orthologs and Bidirectional Best Hits in Bacterial and Archaeal Genomes. <i>Genome Biology and Evolution</i> <b>4</b>, 1286–1294, (2012). <a target='_blank' rel='noopener noreferrer' href="https://doi.org/10.1093/gbe/evs100">doi.org/10.1093/gbe/evs100</a> </li>
				<li> Steinegger, M. & Söding, J. MMseqs2 enables sensitive protein sequence searching for the analysis of massive data sets. <i>Nat Biotechnol</i> <b>35</b>, 1026–1028 (2017). <a target='_blank' rel='noopener noreferrer' href="https://doi.org/10.1038/nbt.3988">doi.org/10.1038/nbt.3988</a></li>
				<li> Kriventseva, E. V. et al. OrthoDB v10: sampling the diversity of animal, plant, fungal, protist, bacterial and viral genomes for evolutionary and functional annotations of orthologs. <i>Nucleic Acids Research</i> <b>47</b>, D807–D811, (2019). <a target='_blank' rel='noopener noreferrer' href="https://doi.org/10.1093/nar/gky1053">doi.org/10.1093/nar/gky1053</a></li>
				<li> Katoh, K. & Standley, D. M. MAFFT Multiple Sequence Alignment Software Version 7: Improvements in Performance and Usability. <i>Molecular Biology and Evolution</i> <b>30</b>, 772–780, (2013). <a target='_blank' rel='noopener noreferrer' href="https://doi.org/10.1093/molbev/mst010">doi.org/10.1093/molbev/mst010</a> </li>
				<li> Edwards, R. J. & Palopoli, N. Computational Prediction of Short Linear Motifs from Protein Sequences. <i>Methods in Molecular Biology Computational Peptidology</i>, 89–141, (2014). <a target='_blank' rel='noopener noreferrer' href="https://doi.org/10.1007/978-1-4939-2285-7_6">doi.org/10.1007/978-1-4939-2285-7_6</a> </li>
				<li> Dosztányi, Z. et al. The Pairwise Energy Content Estimated from Amino Acid Composition Discriminates between Folded and Intrinsically Unstructured Proteins. <i>Journal of Molecular Biology</i> <b>347</b>, 827–839, (2005). <a target='_blank' rel='noopener noreferrer' href="https://doi.org/10.1016/j.jmb.2005.01.071">doi.org/10.1016/j.jmb.2005.01.071</a></li>
				<li> Dosztanyi, Z. et al. IUPred: web server for the prediction of intrinsically unstructured regions of proteins based on estimated energy content. <i>Bioinformatics</i> <b>21</b>, 3433–3434, (2005). <a target='_blank' rel='noopener noreferrer' href="https://doi.org/10.1093/bioinformatics/bti541">doi.org/10.1093/bioinformatics/bti541</a></li>
				<li> Felsenstein, J. Evolutionary trees from DNA sequences: A maximum likelihood approach. <i>Journal of Molecular Evolution</i> <b>17</b>, 368–376, (1981). <a target='_blank' rel='noopener noreferrer' href="https://doi.org/10.1007/bf01734359">doi.org/10.1007/bf01734359</a> </li>
				<li> Guindon, S. et al. New Algorithms and Methods to Estimate Maximum-Likelihood Phylogenies: Assessing the Performance of PhyML 3.0. <i>Systematic Biology</i> <b>59</b>, 307–321, (2010). <a target='_blank' rel='noopener noreferrer' href="https://doi.org/10.1093/sysbio/syq010">doi.org/10.1093/sysbio/syq010</a> </li>
				<li> Minh, B. Q. et al. IQ-TREE 2: New Models and Efficient Methods for Phylogenetic Inference in the Genomic Era. <i>Molecular Biology and Evolution</i> <b>37</b>, 1530–1534 (2020). <a target='_blank' rel='noopener noreferrer' href="https://doi.org/10.1093/molbev/msaa015">doi.org/10.1093/molbev/msaa015</a> </li>
				<li> Hamelryck, T. & Manderick, B. PDB file parser and structure class implemented in Python. <i>Bioinformatics</i> <b>19</b> 2308-10 (2003). <a target='_blank' rel='noopener noreferrer' href="https://doi.org/10.1093/bioinformatics/btg299">doi.org/10.1093/bioinformatics/btg299</a> </li>
				<li> Kabsch, W. & Sander, C. Dictionary of protein secondary structure: pattern recognition of hydrogen-bonded and geometrical features. <i>Biopolymers</i> <b>22</b>, 2577-637 (1983) <a target='_blank' rel='noopener noreferrer' href="https://doi.org/10.1002/bip.360221211">doi.org/10.1002/bip.360221211</a> </li>
				<li> Kunzmann, P. & Hamacher, K. Biotite: a unifying open source computational biology framework in Python. <i>BMC Bioinformatics</i> <b>19</b> 346 (2018). <a target='_blank' rel='noopener noreferrer' href="https://doi.org/10.1186/s12859-018-2367-z">doi.org/10.1186/s12859-018-2367-z</a></li>
				<li> Rodrigues JPGLM, Teixeira JMC, Trellet M, Bonvin AMJJ. pdb-tools: a swiss army knife for molecular structures. <i>F1000Res</i> <b>7</b> 1961 (2018). <a target='_blank' rel='noopener noreferrer' href="https://doi.org/10.12688/f1000research.17456.1">doi.org/10.12688/f1000research.17456.1</a></li>
				<li> Jmol: an open-source Java viewer for chemical structures in 3D. <a target='_blank' rel='noopener noreferrer' href="http://jmol.sourceforge.net/">http://www.jmol.org/</a> </li>
				<li> Rego, N. & Koes, D. 3Dmol.js: molecular visualization with WebGL. <i>Bioinformatics</i> <b>31</b>, 1322–1324 (2015).  <a target='_blank' rel='noopener noreferrer' href="https://doi.org/10.1093/bioinformatics/btu829">doi.org/10.1093/bioinformatics/btu829</a> </li>
			</ol>
						
    </div>
    
    <!-- Include the foot -->
    <?php include("foot.php"); ?>
    
    </body>

</html>
